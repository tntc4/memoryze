# memoryze

Download the executable: **[Link](https://tnt-soft.net/GoDot/memoryze.rar)**

Just extract two files (**Memoryze.exe** and **Memoryze.pck**) in the same folder and run **Memoryze.exe**

Simple memory game made with GoDot Engine v3.1.1

![start game](https://tnt-soft.net/GoDot/Screenshot_1.png "Start Game")

![alt text](https://tnt-soft.net/GoDot/Screenshot_2.png "")

![alt text](https://tnt-soft.net/GoDot/Screenshot_3.png "")

![victory](https://tnt-soft.net/GoDot/Screenshot_4.png "Victory")