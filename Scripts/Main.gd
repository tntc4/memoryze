extends Control

var cards_array:Array = []
var cards_left:Array = []
var max_cards:int
var cards_clicked_left:int
var card1_clicked_index:int
var card2_clicked_index:int
var cards_clicked:Array = []
var game_started:bool
var can_click:bool
var correct:bool
var progress:int

var card_scene:PackedScene = preload("res://Scenes/TexureCard.tscn")

onready var victory_label = $VictoryLabel
onready var board = $Board
onready var progress_bar = $VBoxContainer/DificultyContainer/ProgressBar
onready var begin_btn = $VBoxContainer/BeginButton
onready var dificulty_selector = $VBoxContainer/DificultyContainer/DificultySelector
onready var dificulty_label = $VBoxContainer/DificultyContainer/DificultyLabel
onready var reset_cards_timer = $ResetCardsTimer

const COLOR_HIDDEN:Color = Color(1, 1, 1, 0)
const COLOR_VISIBLE:Color = Color(1, 1, 1, 1)
const COLOR_BLACK:Color = Color(0, 0, 0, 1)

var current_dificulty = 1
var dificulties = {
	1: {
		"text": "Easy",
		"cards_num": 2
	},
	2: {
		"text": "Medium",
		"cards_num": 3
	},
	3: {
		"text": "Hard",
		"cards_num": 4
	},
	4: {
		"text": "Very Hard",
		"cards_num": 5
	}
}

func _ready():
	_hide(victory_label)
	_hide(progress_bar)
	
	init_game()
	update_dificulty()


########
# MAIN #
########
func init_game():
	cards_array.clear()
	cards_left.clear()
	cards_clicked_left = 2
	card1_clicked_index = -1
	card2_clicked_index = -1
	cards_clicked.clear()
	progress_bar.value = 5
	progress = 5
	can_click = false
	correct = false
	
	$ResetCardsTimer.stop()
	$ProgressTimer.stop()
	$GameOverTimer.stop()
	
	generate_cards()


func start_game():
	_hide(victory_label)
	_hide(progress_bar)
	
	_hide(dificulty_label, true)
	_hide(dificulty_selector, true)
	
	# reveal cards
	var cards = board.get_children()
	for i in cards:
		i.modulate = COLOR_VISIBLE
	
	_show(progress_bar)
	$ProgressTimer.start()


func update_dificulty():
	var dificulty_data = get_dificulty_data(current_dificulty)
	
	dificulty_selector.value = current_dificulty
	dificulty_label.text = dificulty_data["text"]


func get_dificulty_data(dificulty:int):
	var return_data = {
		"text": "Easy",
		"cards_num": 2
	}
	
	if dificulty >= 1 && dificulty <= 4:
		return_data = dificulties[dificulty]
	
	return return_data


func generate_cards():
	var dificulty_data = get_dificulty_data(current_dificulty)
	max_cards = dificulty_data["cards_num"]
	
	# remove cards from previous game sessions
	if board.get_child_count() > 0:
		var old_cards = board.get_children()
		
		for i in old_cards:
			i.queue_free()
			yield(i, "tree_exited")
	
	board.rect_position.x = 480
	board.rect_position.y = 182
	board.rect_size.x = 64
	board.rect_size.y = 64
	board.rect_min_size.x = 64
	board.rect_min_size.y = 64
	board.columns = 1
	
	# reset variables
	cards_left.clear()
	cards_array.clear()
	
	randomize()
	# generate array with cards
	for i in range(1, max_cards + 1):
		# shuffle cards
		if cards_array.size() > 2:
			cards_array.shuffle()
		
		# append card instance 1
		cards_array.append(i)
		
		# shuffle cards
		if cards_array.size() > 2:
			cards_array.shuffle()
		
		# append cards instance 2
		cards_array.append(i)
		
		# shuffle cards
		if cards_array.size() > 2:
			cards_array.shuffle()
		
		# append card value to the "cards left" array
		cards_left.append(i)
	
	# add cards to the board
	for i in cards_array:
		var tmp_node:TextureButton = card_scene.instance()
		tmp_node.name = "Card" + str(i)
		tmp_node.find_node("Texture").texture = load("res://Resources/Images/" + str(i) + ".jpg")
		tmp_node.card_value = i
		tmp_node.modulate = COLOR_BLACK
		
		board.add_child(tmp_node, true)
		tmp_node = null
	
	board.columns = max_cards


func _show(el):
	el.modulate = COLOR_VISIBLE
	el.show()
	
	if el is BaseButton:
		el.disabled = false
	elif el is Slider:
		el.editable = true


func _hide(el, true_hide:bool = false):
	if true_hide == true:
		el.hide()
	else:
		el.modulate = COLOR_HIDDEN
	
	if el is BaseButton:
		el.disabled = true
	elif el is Slider:
		el.editable = false


func _on_DificultySelector_value_changed(value):
	current_dificulty = value
	update_dificulty()
	generate_cards()


func _on_BeginButton_pressed():
	if game_started == false:
		_hide(begin_btn)
		game_started = true
		start_game()


func _on_ProgressTimer_timeout():
	if progress > 0:
		progress = progress - 1
		progress_bar.value = progress
		$ProgressTimer.start()
	else:
		# start the game
		_hide(progress_bar)
		
		# hide cards
		var cards = board.get_children()
		for i in cards:
			i.modulate = COLOR_BLACK
			i.mouse_default_cursor_shape = CURSOR_POINTING_HAND
			i.connect("pressed", self, "_on_card_pressed", [i])
		
		can_click = true

func _on_ResetCardsTimer_timeout():
	if cards_clicked.size() > 0:
		var card1:TextureButton = board.get_child(cards_clicked.pop_front())
		var card2:TextureButton = board.get_child(cards_clicked.pop_front())
		
		# hide card 1
		card1.modulate = COLOR_BLACK
		card1.disabled = false
		card1.clicked = false
		card1.mouse_default_cursor_shape = CURSOR_POINTING_HAND
		
		# hide card 2
		card2.modulate = COLOR_BLACK
		card2.disabled = false
		card2.clicked = false
		card2.mouse_default_cursor_shape = CURSOR_POINTING_HAND
		
		if cards_clicked.size() > 0:
			reset_cards_timer.start()


func _on_GameOverTimer_timeout():
	# show dificulty selector
	_show(dificulty_label)
	_show(dificulty_selector)
	
	# show begin button
	_show(begin_btn)
	
	init_game()


func _on_card_pressed(card:TextureButton):
	if game_started == true:
		if can_click == true:
			if cards_clicked_left == 2:
				cards_clicked_left = 1
				card1_clicked_index = card.get_index()
				
				card.modulate = COLOR_VISIBLE
				card.disabled = true
				card.clicked = true
				card.mouse_default_cursor_shape = CURSOR_ARROW
			elif cards_clicked_left == 1:
				cards_clicked_left = 0
				card2_clicked_index = card.get_index()
				
				card.modulate = COLOR_VISIBLE
				card.disabled = true
				card.clicked = true
				card.mouse_default_cursor_shape = CURSOR_ARROW
				
				var value1 = board.get_child(card1_clicked_index).card_value
				var value2 = board.get_child(card2_clicked_index).card_value
				
				# correct
				if value1 == value2:
					correct = true
					cards_left.erase(value1)
					
					# check for end game
					if cards_left.size() == 0:
						# no cards left - show victory label and begin button
						game_over()
				# incorrect
				else:
					correct = false
					if reset_cards_timer.is_stopped() == true:
						reset_cards_timer.start()
					
					cards_clicked.append(card1_clicked_index)
					cards_clicked.append(card2_clicked_index)
				
				cards_clicked_left = 2
				card1_clicked_index = -1
				card2_clicked_index = -1


func game_over():
	_show(victory_label)
	
	$ResetCardsTimer.stop()
	
	game_started = false
	
	$GameOverTimer.start()
